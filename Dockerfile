FROM openjdk:11-jdk-slim
ARG JAR_FILE=target/demomaven-0.0.3-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
